
def reading_file(f):
    """

    """

    f = open(f).readlines()
    R, S, U, P, Serv = [int(x) for x in f[0].strip().split()]  # Rows, Slots, Unavailable, Pools and Servers

    Unavailable = [tuple(int(x) for x in f[i].strip().split()) for i in range(1,U+1)]

    #     pizza = np.array(
    #         [list(map(lambda y: 0 if y == 'M' else 1, x.strip())) for x in f[1:]])

    Servers = [tuple(int(x) for x in f[i].strip().split()) for i in range(U+1, Serv+2)]
    return R, S, U, P, Serv, Unavailable, Servers

reading_file("dc.in")
# reading_file("test.in")
