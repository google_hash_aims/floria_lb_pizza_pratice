#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 11 12:06:35 2019

@author: sbk
"""

import numpy as np
import math
from copy import deepcopy


def transform_pizza(f):
    """ 
    This function takes in the pizza input file and
    transforms it into a numpy array of 0s and 1s, 
    where 0==T and 1==M
    R -> rows of pizza
    C -> cols of pizza
    m -> min of each T and M per slice
    M -> max size of slice
    """

    f = open(f).readlines()
    constraints = tuple(int(x) for x in f[0].strip().split())  # R, C, m, M
    pizza = np.array(
        [list(map(lambda y: 0 if y == 'M' else 1, x.strip())) for x in f[1:]])
    return pizza, constraints


class PizzaSlicer:
    """
    The uses a simple heuristic to make pizza slices and is guranteed to give 
    optimal solution, or maybe not.

    Heuristic
    ======
    1. Take slices of pizza from the coners and towards the center
    2. Take the smallest slice that satisfies the constraints
    3.This requires that after taking all the possible slices using 1,2
    we have to go through all the slices and expand to largest possible size.
    """

    def __init__(self, pizzac):
        """ 
        This function returns all the slices of pizza in the required format
        :pizzac: a tuple containing the pizza to be sliced and the constraints
        """
        c = pizzac[1]
        self.pizza = pizzac[0]
        self.min_cut = 2*c[2]
        self.max_cut = c[3]
        self.locs = []  # store all used rows and cols
        self.pcuts = range(self.min_cut, self.max_cut+1)
        self.pizza_shape = self.pizza.shape
        self.state = 0  # four states 0,1,2,3
        self.outer = np.array([(0, 0), (0, self.pizza_shape[1]), (self.pizza_shape[0], 0),
                               (self.pizza_shape[0], self.pizza_shape[1])])
        self.inner = np.zeros(shape=(4, 1), dtype=int)
        self.inner_timer = 0
        self.score = 0

    def outer_to_inner(self):
        self.outer[:, 0] = self.inner[:, 0]

    def prepare_outer(self, r):
        if self.inner_timer < 4:
            self.inner[self.inner_timer] = r
            self.inner_timer += 1

    def cross(self, row, col):
        """
        Given row and col, return a set of their cross products
        """
        return set([(r, c) for r in range(*row) for c in range(*col)])

    def slice_free(self, slice_cover):
        """
        Try to determine that this slice does not intersect with any other slice
        """
        for cut in self.locs:
            cut_cover = self.cross(cut[0], cut[1])
            # intersects with another slice
            if not slice_cover.isdisjoint(cut_cover):
                return False  # this slice is not free
        return True

    def slice_valid(self, pslice):
        return min(pslice.size-pslice.sum(), pslice.sum()) >= self.min_cut/2

    def next_loc(self):
        """
        Try to determine where to cut next
        """
        return self.outer[self.state]

    def row_add(self, r, a):
        if self.state in [0, 1]:
            return r+a
        else:
            return r-a

    def col_add(self, c, a):
        if self.state in [0, 2]:
            return c+a
        else:
            return c-a

    def slice_pizza(self, loc, shape):
        # check if slice position is available
        r1, c1 = loc
        r2 = self.row_add(r1, shape[0])
        c2 = self.col_add(c1, shape[1])
        if r2 < 0 or c2 < 0:
            return False,
        if r2 > self.pizza_shape[0] or c2 > self.pizza_shape[1]: 
            return False,
        cover = self.cross((min(r1, r2), max(r1, r2)),
                           (min(c1, c2), max(c1, c2)))
        if self.slice_free(cover):
            return (True, [tuple(sorted((r1, r2))), tuple(sorted((c1, c2))), (r1, c1), self.state],
                    self.pizza[min(r1, r2):max(r1, r2), min(c1, c2):max(c1, c2)], r2)
        else:
            return False,

    def orients(self, size):
        """
        orients(1000) = ((5, 20), (4, 25), (2, 50), (1, 100))
        """
        divs = [x for x in range(1, size//2+1) if size % x == 0]+[size]
        return tuple(zip(divs[:math.ceil(len(divs)/2)], divs[-1:math.ceil(len(divs)//2)-1:-1]))

    def one_cut(self, loc):
        """
        This function manages to take a slice of pizza
        """
        # all this to make just one cut
        for size in self.pcuts:
            # generate the possible ways to cut with size
            gen = self.orients(size)
            for shape in reversed(gen):
                # try to make slice (a,b)
                pslice = self.slice_pizza(loc, shape)
                if pslice[0]:
                    # check if slice meets constraints
                    if self.slice_valid(pslice[2]):
                        self.locs.append(pslice[1])
                        self.outer[self.state][1] = pslice[1][1][1]
                        self.prepare_outer(pslice[3])
                        return True

                if tuple(reversed(shape)) == shape:
                    continue

                # try to make slice other way (b,a)
                pslice = self.slice_pizza(loc, tuple(reversed(shape)))
                if pslice[0]:
                    # check again if slice meets constraints
                    if self.slice_valid(pslice[2]):
                        self.locs.append(pslice[1])
                        self.outer[self.state][1] = pslice[1][1][1]
                        self.prepare_outer(pslice[3])
                        return True
        return False

    def expand_slice(self, loc):
        """
        Try to expand an existing slice
        """
        # all this to make just one cut
        for size in reversed(self.pcuts):
            # generate the possible ways to cut with size
            gen = self.orients(size)
            for shape in reversed(gen):
                # try to make slice (a,b)
                pslice = self.slice_pizza(loc, shape)
                if pslice[0]:
                    # check if slice meets constraints
                    if self.slice_valid(pslice[2]):
                        self.locs.append(pslice[1])
                        return True

                if tuple(reversed(shape)) == shape:
                    continue

                # try to make slice other way (b,a)
                pslice = self.slice_pizza(loc, tuple(reversed(shape)))
                if pslice[0]:
                    # check again if slice meets constraints
                    if self.slice_valid(pslice[2]):
                        self.locs.append(pslice[1])
                        return True
        return False

    def phase_one(self):
        """
        Just take slices following rules in 1,2
        We can tell if there is no more space for slicing by
        recording 8 consecutive false from self.one_cut
        """
        count = 0
        n = 0
        while True:
            self.state = count % 4
            loc = self.next_loc()  # determine next place to cut from
            if self.one_cut(loc):
                n = 0
            else:
                n += 1
                if n == 4:
                    self.outer_to_inner()
                    self.inner_timer = 0
                    # switch

                if n == 8:
                    break
            count += 1

    def phase_two(self):
        """
        Expand existing slices
        """
        locs = deepcopy(self.locs)
        for i, cut in enumerate(locs):
            self.locs.remove(cut)
            self.state = cut[3]
            self.expand_slice(cut[2])

    def slicer(self):
        self.phase_one()
        self.phase_two()

    def calc_score(self):
        self.score = 0
        for cut in self.locs:
            size = (cut[0][1]-cut[0][0])*(cut[1][1]-cut[1][0])
            self.score += size
        return self.score


ps = PizzaSlicer(transform_pizza('../a_example.in'))
# ps = PizzaSlicer(transform_pizza('../d_big.in'))

ps.phase_one()
print(f"score phase 1 = {ps.calc_score()}")
ps.phase_two()
print(f"score phase 2 = {ps.calc_score()}")
